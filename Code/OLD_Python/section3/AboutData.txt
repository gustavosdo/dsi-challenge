Brazilian E-Commerce Public Dataset by Olist
Welcome! This is a Brazilian ecommerce public dataset of orders made at Olist Store. The dataset has information of 100k orders from 2016 to 2018 made at multiple marketplaces in Brazil. Its 21 supportive features allows viewing an order from multiple dimensions: from order status, price and freight performance to customer location, product attributes and finally reviews written by customers.

We also released a geolocation dataset that relates Brazilian zip codes to lat/lng coordinates and a dataset with the payment methods chosen at each order.

This is real commercial data, it has been anonymised, and references to the companies and partners in the review text have been replaced with the names of Game of Thrones great houses.

Context
This dataset was generously provided by Olist, the largest department store in Brazilian marketplaces. Olist connects small businesses from all over Brazil to channels without hassle and with a single contract. Those merchants are able to sell their products through the Olist Store and ship them directly to the customers using Olist logistics partners.

After a customer purchases the product from Olist Store a seller gets notified to fulfill that order. Once the customer receives the product, or the estimated delivery date is due, the customer gets a satisfaction survey by email where he can give a note for the purchase experience and write down some comments.

Attention
Note that a comment may be repeated if an order has two or more different products.
An order may also be fulfilled by more than one seller if the customer purchases mor than one product.
Some review comments had personal data like phone numbers, so we did a regex search replacing every group of 3 numbers by '000'. This might mess up with some data other than phone numbers in the comments.
All text identifying stores and partners where replaced by the names of Game of Thrones great houses.
Datasets
We are releasing two independent datasets intended for different types of studies. Each row in those datasets corresponds to a customer order, product and review.

Unclassified Orders Dataset
This dataset includes 100k rows and 21 feature variables.

olist_public_dataset_v2.csv

Geolocation Dataset
This dataset includes random latitudes and longitudes from a given zip code prefix.

geolocation_olist_public_dataset.csv

Customers Dataset
This dataset includes unique identifiers of customers.

olist_public_dataset_v2_customers.csv

Payments Dataset
This dataset includes data about the payment options from orders.

olist_public_dataset_v2_payments.csv

Classified Dataset
This dataset includes 3,584 rows, the same 21 feature variables from the unclassified dataset plus some information about its classification.

olist_classified_public_dataset.csv

This dataset was classified by three independent analysts. Each analyst voted for a class that he thinks a comment should bellong to. After that we have classified a comment by chosing the most voted class.

Inspiration
Here are some inspiration for possible outcomes from this dataset.

NLP: 
This dataset offers a supreme environment to parse out the reviews text through its multiple dimensions.

Clustering:
Some customers didn't write a review. But why are they happy or mad?

Sales Prediction:
With purchase date information you'll be able to predict future sales.

Delivery Performance:
You will also be able to work through delivery performance and find ways to optimize delivery times.

Product Quality: 
Enjoy yourself discovering the products categories that are more prone to customer insatisfaction.

Feature Engineering: 
Create features from this rich dataset or attach some external public information to it.

Acknowledgements
Thanks to Olist for releasing this dataset.
