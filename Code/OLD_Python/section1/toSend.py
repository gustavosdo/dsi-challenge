import csv, math, datetime
allFiles = [\
'2009Q3-house-disburse-detail.csv',\
'2009Q4-house-disburse-detail.csv',\
'2010Q1-house-disburse-detail.csv',\
'2010Q2-house-disburse-detail.csv',\
'2010Q3-house-disburse-detail.csv',\
'2010Q4-house-disburse-detail.csv',\
'2011Q1-house-disburse-detail.csv',\
'2011Q2-house-disburse-detail.csv',\
'2011Q3-house-disburse-detail.csv',\
'2011Q4-house-disburse-detail.csv',\
'2012Q1-house-disburse-detail.csv',\
'2012Q2-house-disburse-detail.csv',\
'2012Q3-house-disburse-detail.csv',\
'2012Q4-house-disburse-detail.csv',\
'2013Q1-house-disburse-detail.csv',\
'2013Q2-house-disburse-detail.csv',\
'2013Q3-house-disburse-detail.csv',\
'2013Q4-house-disburse-detail.csv',\
'2014Q1-house-disburse-detail.csv',\
'2014Q2-house-disburse-detail.csv',\
'2014Q3-house-disburse-detail.csv',\
'2014Q4-house-disburse-detail.csv',\
'2015Q1-house-disburse-detail.csv',\
'2015Q2-house-disburse-detail-updated.csv',\
'2015Q3-house-disburse-detail.csv',\
'2015Q4-house-disburse-detail.csv',\
'2016Q1-house-disburse-detail.csv',\
'2016Q2-house-disburse-detail.csv',\
'2016Q3-house-disburse-detail.csv',\
'2016Q4-house-disburse-detail.csv',\
'2017Q1-house-disburse-detail.csv',\
'2017Q2-house-disburse-detail.csv',\
'2017Q3-house-disburse-detail.csv',\
'2017Q4-house-disburse-detail.csv',\
'2018Q1-house-disburse-detail.csv']

# Variables
sumPayments = 0. # sum off all payments
nPayments = 0 # number of all payments
amountIndex = 0 # 'AMOUNT' column
# Code
for detailedFile in allFiles: # opening file-by-file on list above
	with open(detailedFile, 'r', encoding = "ISO-8859-1") as quarter: # read the file
		expenditure = csv.reader(quarter, delimiter=',') # import all text from file to 'expenditure'
		for entry in expenditure: # entry = line from file
			nLine += 1
			for info in entry: # entry is a list of infos (such as AMOUNT, START DATE, etc.)
				if info == 'AMOUNT':
					if detailedFile != '2017Q2-house-disburse-detail.csv':
						amountIndex = entry.index(info)
					else:
						amountIndex = entry.index(info) - 1 # bug on 2017Q2 file
			if entry[amountIndex] not in ['AMOUNT', 'PURPOSE']:
				sumPayments += float(entry[amountIndex].replace(",", ""))
				nPayments += 1
# Printing results
print("Sum of all payments = %f" %sumPayments)
print("Number of payments = %d" %nPayments)
