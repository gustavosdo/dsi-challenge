#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# csv to read files; math library 
import csv, math, datetime
# all expenditure/quarter csv files
allFiles = [\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2009Q3-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2009Q4-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2010Q1-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2010Q2-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2010Q3-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2010Q4-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2011Q1-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2011Q2-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2011Q3-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2011Q4-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2012Q1-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2012Q2-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2012Q3-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2012Q4-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2013Q1-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2013Q2-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2013Q3-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2013Q4-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2014Q1-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2014Q2-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2014Q3-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2014Q4-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2015Q1-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2015Q2-house-disburse-detail-updated.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2015Q3-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2015Q4-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2016Q1-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2016Q2-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2016Q3-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2016Q4-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2017Q1-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2017Q2-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2017Q3-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2017Q4-house-disburse-detail.csv',\
'/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2018Q1-house-disburse-detail.csv']

#allFiles = ['/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2009Q3-house-disburse-detail.csv']

# Variables
sumPayments = 0. # sum off all payments
nPayments = 0 # number of all payments
amountIndex = 0 # 'AMOUNT' column
startDateIndex = 0 # 'START DATE' column
endDateIndex = 0 # 'END DATE' column
startDate = datetime.date(1,1,1) # initialize start date variable
endDate = datetime.date(1,1,1) # initialize end date variable
coveragePeriods = [] # days between END DATE and START DATE for strict positive AMOUNTS
nLine = 0
# Code
for detailedFile in allFiles: # opening file-by-file on list above
	with open(detailedFile, 'r', encoding = "ISO-8859-1") as quarter: # read the file
		expenditure = csv.reader(quarter, delimiter=',') # import all text from file to 'expenditure'
		for entry in expenditure: # entry = line from file
			nLine += 1
			for info in entry: # entry is a list of infos (such as AMOUNT, START DATE, etc.)
				if info == 'AMOUNT':
					if detailedFile != '/home/gustavo/Dropbox/GitLab/dsi-challenge/Data/house-office-expenditures-with-readme/house-office-expenditures-with-readme/2017Q2-house-disburse-detail.csv':
						amountIndex = entry.index(info)
					else:
						amountIndex = entry.index(info) - 1 # bug on 2017Q2 file
				#if info == 'START DATE':
				#	startDateIndex = entry.index(info)
				#if info == 'END DATE':
				#	endDateIndex = entry.index(info)
			if entry[amountIndex] not in ['AMOUNT', 'PURPOSE']:
				sumPayments += float(entry[amountIndex].replace(",", ""))
				nPayments += 1
			#	if ( float(entry[amountIndex].replace(",", "")) > 0 ) and ( entry[startDateIndex] != "" ) and ( entry[endDateIndex] != "" ):
			#		strStartDate = str(entry[startDateIndex])
			#		startDate = datetime.datetime.strptime(strStartDate, '%m/%d/%y')
			#		strEndDate = str(entry[endDateIndex])
			#		endDate = datetime.datetime.strptime(strEndDate, '%m/%d/%y')
			#		print(' * * * * * ')
			#		print('File: '+detailedFile+' ; Amount column: %d ; Start Date column: %d ; End Date: %d' %(amountIndex,startDateIndex,endDateIndex))
			#		print('nLine = ', nLine)
			#		print(startDate, endDate)
		#nLine = 0
		#if entry[amountIndex] not in ['AMOUNT', 'PURPOSE'] and float(entry[amountIndex].replace(",", "")) > 0:
		#	coveragePeriods.append(
# Printing results
print("Sum of all payments = %f" %sumPayments)
print("Number of payments = %d" %nPayments)
