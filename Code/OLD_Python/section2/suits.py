import math, random, numpy

G = 1000 # number of 'games'
N = 26 # total of cards
M = 2  # suits
pGame = 0  # points at the end
Pall = []
# deck with al cards
deck = []

for game in range(0,G):
	for suit in range(0,M):
		subDeck = [suit for i in range(0, N//M)]
		#print(subDeck)
		deck += subDeck
	
	random.shuffle(deck)
	#print(deck)
	for card in range(0,N):
		if (card > 0) and (deck[card] == deck[card-1]): pGame += 1
	Pall.append(pGame)
	pGame = 0

#print(Pall)
print(numpy.mean(Pall))
