
Decks = function(cfg){
  
  # Questions statement --------------------------------------------------------
  # You have a deck with N different playing cards, equally distributed amongst M
  # suits. You draw all cards without putting any back in the deck. After drawing
  # the first card, you compare the suit of each subsequent card drawn with the
  # suit of the card drawn immediately before. If the suits match, you get a
  # point. Otherwise, you get no points. Please answer the following questions
  # about the total number of points, P, at the end of the process.
  
  # Problem 1 ------------------------------------------------------------------
  # What is the mean of P when N=26 and M=2?
  
  # Number of cards per suit
  N = 26
  
  # Number of suits
  M = 2
  
  # Possible suits
  suits = c("diamond", "club", "hearts", "spades")
  
  # Subset number of suits for a given M
  sub_suits = sample(x = suits, size = M)
  
  # Number of games
  G = 1000000
  
  # Points per game
  P = c()
  
  for (iter_game in 1:G){
    
    # Build the deck
    deck = sample(unlist(lapply(1:M, function(m){rep(sub_suits[m], N/M)})))
    
    # Playing the game! --------------------------------------------------------
    # Number of points
    iter_P = 0
    
    # previous card
    p_card = deck[1]
    
    for (card in deck[2:length(deck)]){
      # Assigning a point in case the cards match
      if (card == p_card) iter_P = iter_P + 1
      # Updating card in hand
      p_card = card
    }
    # Appending result to Points vector
    P[iter_game] = iter_P
  }
}