
ProcessExpenditures = function(all.data, cfg){
  
  # Question 1 -----------------------------------------------------------------
  # What is the total of all the payments in the dataset?
  # All amounts
  all.amounts = as.numeric(gsub(x = all.data$AMOUNT,
                                pattern = ",",
                                replacement = ""))
  
  # Is there NA's in all.amounts
  na.rm = (length(which(is.na(all.amounts))) > 0)
  # Solution
  q1 = as.numeric(format(sum(as.numeric(all.amounts), na.rm = na.rm),
                         scientific = T))
  # Statistics
  #print(summary(all.amounts))
  
  # Strictly positive amounts
  pos.amounts = all.amounts[all.amounts > 0]
  # Is there NA's in all.amounts
  na.rm = (length(which(is.na(pos.amounts))) > 0)
  # Solution
  q1.pos = as.numeric(format(sum(as.numeric(pos.amounts), na.rm = na.rm),
                             scientific = T))
  # Statistics
  #print(summary(pos.amounts))
  
  # Question 2 -----------------------------------------------------------------
  # Define the 'COVERAGE PERIOD' for each payment as the difference (in days)
  # between 'END DATE' and 'START DATE'. What is the standard deviation in
  # 'COVERAGE PERIOD'? Only consider payments with strictly positive amounts.
  
  # Strictly positive (removing NAs)
  all.data.pos.amounts = all.data[(all.data$AMOUNT > 0) &
                                    !is.na(all.data$START.DATE) &
                                    !is.na(all.data$END.DATE), ]
  
  # Calculating coverage period
  cov.period = as.numeric(
    as.Date.character(x = as.character(all.data.pos.amounts$END.DATE),
                      format = "%m/%d/%y") -
      as.Date.character(x = as.character(all.data.pos.amounts$START.DATE),
                        format = "%m/%d/%y"))
  
  # There are some empty values in START.DATE and END.DATE
  # The conversion made above brings NAs in these cases
  cov.period = cov.period[!is.na(cov.period)]
  
  # Removing negative cov.period (not reasonable cases)
  cov.period = cov.period[cov.period >= 0]
  
  # Calculating standard deviation
  mean.cov.period = sum(cov.period)/length(cov.period)
  cov.period.mean.remove = cov.period - rep(mean.cov.period, length(cov.period))
  q2 = sqrt((1/length(cov.period)) * sum(cov.period.mean.remove**2))
  
  # Question 3 -----------------------------------------------------------------
  # What was the average annual expenditure with a 'START DATE' date between
  # January 1, 2010 and December 31, 2016 (inclusive)? Only consider payments
  # with strictly positive amounts.
  
  # Section: Expenditures within the given range
  # Converting START.DATE variable 
  all.data.pos.amounts$START.DATE = as.Date.character(
    as.character(
      all.data.pos.amounts$START.DATE), format = "%m/%d/%y")
  # Removing NAs
  if ( length(which(is.na(all.data.pos.amounts$START.DATE))) > 0 ){
    na.ptr = is.na(all.data.pos.amounts$START.DATE)
    all.data.pos.amounts = all.data.pos.amounts[!na.ptr,]
  }
  # Pointer to entries within considered range
  init.date = as.Date.character("2010-01-01")
  end.date = as.Date.character("2016-12-31")
  date.ptr = (all.data.pos.amounts$START.DATE >= init.date) &
    (all.data.pos.amounts$START.DATE <= end.date)
  # Entries with positive amount and within range
  data.pos.range = all.data.pos.amounts[date.ptr,]
  # Preprocessing YEAR column
  # Removing NA's from YEAR column
  data.pos.range = data.pos.range[!(is.na(data.pos.range$YEAR)),]
  # Removing undesirable strings in YEAR column (FISCAL YEAR)
  year.entries = unique(data.pos.range$YEAR)
  years = c()
  for (iter in 1:length(year.entries)){
    if (!is.na(as.numeric(year.entries[iter]))){
      years[iter] = as.numeric(year.entries[iter])
    } else {
      chars = unlist(strsplit(x = year.entries[iter], split = ""))
      numbers = chars[!is.na(as.numeric(chars))]
      year = paste0(numbers, collapse = "")
      years[iter] = as.numeric(year)
    }
  }
  data.pos.range$YEAR = sapply(1:length(data.pos.range$YEAR),
                               function(x){
                                 years[year.entries %in% data.pos.range$YEAR[x]]
                               })
  # Total expenditure for each year
  total = list()
  for (iter.year in unique(years)){
    iter.data = data.pos.range[data.pos.range$YEAR == iter.year,]
    total[[as.character(iter.year)]] = sum(as.numeric(gsub(x = iter.data$AMOUNT,
                                                           pattern = ",",
                                                           replacement = "")))
  }
  # Mean of annual expenditures
  q3 = mean(unlist(total))
  
  # Question 4 -----------------------------------------------------------------
  #Find the 'OFFICE' with the highest total expenditures with a 'START DATE' in
  #2016. For this office, find the 'PURPOSE' that accounts for the highest total
  #expenditures. What fraction of the total expenditures (all records, all
  #offices) with a 'START DATE' in 2016 do these expenditures amount to?
  
  # Update START.DATE for a date format
  all.data$START.DATE = as.Date.character(as.character(all.data$START.DATE),
                                          format = "%m/%d/%y")
  # 2016 entries
  init.date = as.Date.character("2016-01-01")
  end.date = as.Date.character("2016-12-31")
  date.ptr = (all.data$START.DATE >= init.date) & (all.data$START.DATE <= end.date)
  data.2016 = all.data[date.ptr,]
  # All OFFICES
  data.2016$OFFICE = as.character(data.2016$OFFICE)
  offices = unique(data.2016$OFFICE)
  # Parallelism setup
  threads = cfg$expenditures$threads
  cl <- parallel::makeCluster(threads, type = 'SOCK', outfile = "")
  doParallel::registerDoParallel(cl)
  on.exit(stopCluster(cl))
  # Total amount for each office
  total = unlist(foreach (iter.office = offices[1:length(offices)]) %dopar% {
    iter.data = data.2016[data.2016$OFFICE == iter.office,]
    return(sum(as.numeric(gsub(x = iter.data$AMOUNT,
                               pattern = ",",
                               replacement = "")),
               na.rm = T))
  })
  # Highest total amount office
  high.amount.off = offices[total == max(total)]
  # All entries from this office
  off.data = data.2016[data.2016$OFFICE == high.amount.off,]
  purposes = na.omit(unique(off.data$PURPOSE))
  # Total amount for each purpose
  total = unlist(foreach (iter.purpose = purposes[1:length(purposes)]) %dopar% {
    iter.data = off.data[off.data$PURPOSE == iter.purpose,]
    return(sum(as.numeric(gsub(x = iter.data$AMOUNT,
                               pattern = ",",
                               replacement = "")),
               na.rm = T))
  })
  # Highest total amount purpose for that given office
  high.amount.purpose = purposes[total == max(total)]
  # Amount
  high.amount = sum(as.numeric(
    gsub(x = off.data$AMOUNT[off.data$PURPOSE == high.amount.purpose],
         pattern = ",",
         replacement = "")),
    na.rm = T)
  # Total expenditures of 2016
  total.amount = sum(as.numeric(gsub(x = data.2016$AMOUNT,
                                     pattern = ",",
                                     replacement = "")),
                     na.rm = T)
  # Fraction
  q4 = high.amount/total.amount
  
  # Question 5 -----------------------------------------------------------------
  # What was the highest average staff salary among all representatives in 2016?
  # Assume staff sizes is equal to the number of unique payees in the 'PERSONNEL
  # COMPENSATION' category for each representative.
  
  # Removing undesirable strings in YEAR column (FISCAL YEAR)
  year.entries = unique(all.data$YEAR)
  years = c()
  for (iter in 1:length(year.entries)){
    if (!is.na(as.numeric(year.entries[iter]))){
      years[iter] = as.numeric(year.entries[iter])
    } else {
      chars = unlist(strsplit(x = year.entries[iter], split = ""))
      numbers = chars[!is.na(as.numeric(chars))]
      year = paste0(numbers, collapse = "")
      years[iter] = as.numeric(year)
    }
  }
  all.data$YEAR = sapply(1:length(all.data$YEAR),
                               function(x){
                                 years[year.entries %in% all.data$YEAR[x]]
                               })
  # 2016 data
  data.2016 = all.data[all.data$YEAR == 2016,]
  # Unique representatives
  reps = na.omit(unique(data.2016$BIOGUIDE_ID))
  for (rep in reps){
    purpose = "PERSONNEL COMPENSATION TOTALS:.*"
    iter.data = data.2016[data.2016$BIOGUIDE_ID == rep,]
    #gsub(pattern = purpose, )
  }
  
  
  # Question 6 -----------------------------------------------------------------
  # What was the median rate of annual turnover in staff between 2011 and 2016
  # (inclusive)? Turnover for 2011 should be calculated as the fraction of a
  # representative's staff from 2010 who did not carry over to 2011. Only
  # consider representatives who served for at least 4 years and had staff size
  # of at least 5 every year that they served.
  
  # Question 7 -----------------------------------------------------------------
  # What percentage of the expenditures of the top 20 spenders in 2016 come from
  # members of the Democratic Party? Representatives are identified by their
  # 'BIOGUIDE_ID', which can be used to look up representatives with
  # ProPublica's Congress API to find their party affiliation. Consider an
  # expenditure as being in 2016 if its 'START DATE' is in 2016.
  # API: https://projects.propublica.org/api-docs/congress-api/members/#get-a-specific-member
  
  # All solutions ----
  return(list(q1 = q1, q2 = q2, q3 = q3, q4 = q4))
}